# BI ML - Technical challenge

Reto técnico para el tech team de BI ML en Mercado Libre.

Este reto consiste de dos partes, una técnica y una teórica, encontradas en las carpetas desafio_tecnico y desafio_teorico, respectivamente.
Cada carpeta tiene un README.md específico. Las respuestas a las preguntas teóricas están en `desafio_teorico/README.md` y todas las consideraciones sobre el reto técnico están en `desafio_tecnico/README.md`.

Para correr este proyecto se necesita tener docker instalado.
Con correr dos comandos es suficiente:
1. `docker-compose build`
2. `docker-compose up`

Esto deja corriendo el API en `http://localhost:8080/`.

Para ver la documentación del API se puede ingresar a `http://localhost:8080/docs/`.

Para correr el ejercicio está el endpoint `https://localhost:8080/items` con el método POST. Puede correrse sin parámetros, o especificar un body en la petición de la siguiente manera:

```json
{
    "batch": "250",
    "encoding": "utf-8",
    "separator": ",",
    "format": ".jsonl",
    "path": "data/technical_challenge_data_small"
}
```

> Más información en `http://localhost:8080/docs` y en `desafio_tecnico/README.md`.


### Limitaciones (tareas no implementadas o no terminadas e ideas no exploradas):

* Terminar la conexión a la base de datos y su respectivo cursor.
* Explorar estrategias de paralelismo para leer e insertar de manera más eficiente utilizando, por ejemplo, threads.
* Hacer la lectura del archivo por lotes para poder leer un archivo más grande que la memoria disponible y que no se pueda cargar completamente a memoria. Esto se puede combinar con el uso de paralelismo para poder leer y procesar al mismo tiempo.
* Mejorar la estrategia de ramas del repositorio para GitLab CI/CD, por ejemplo se puede tener una rama `dev` a donde se suban los cambios, y se corran las pruebas antes de llegar a `master`.
* Usar GitLab Container Registry para tener imágenes con `docker`, `docker-compose` y `python3.7` instalado para hacer los tests más rápidos.
* Devolver los ítems de la base de datos como diccionario/json con los nombres de los campos


