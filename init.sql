 CREATE TABLE biml
    (
        id TEXT,
        site TEXT,
        price INTEGER,
        currency TEXT,
        start_time TIMESTAMP,
        category TEXT,
        seller TEXT,
        PRIMARY KEY(id)
    );
