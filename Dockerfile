FROM python:3.7

# USER app
ENV PYTHONBUFFERED 1
# RUN mkdir /db
#RUN chown app:app -R /db

WORKDIR /code/
ADD ./desafio_tecnico/requirements.txt /code/desafio_tecnico/
RUN pip install -r desafio_tecnico/requirements.txt

ADD ./desafio_tecnico /code/desafio_tecnico
ADD ./data /code/data

ENV PYTHONPATH="."

#CMD python ./app/api/main.py
