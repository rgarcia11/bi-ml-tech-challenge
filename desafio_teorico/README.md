# Desafío Teórico

> ### Consigna
>
> #### Procesos, hilos y corrutinas
>
> - Un caso en el que usarías procesos para resolver un problema y por qué.
> - Un caso en el que usarías threads para resolver un problema y por qué.
> - Un caso en el que usarías corrutinas para resolver un problema y por qué.

Es posible que como desarrollador, dependiendo del lenguaje que se maneje, no se tengan las tres opciones disponibles, o sea transparente al momento de desarrollar.

Por ejemplo en Python hay librerías que ofrecen abstracciones de alguna de estas tres estrategias. En el siguiente punto se utilizan dos librerías que utilizan threads y corrutinas, pero el desarrollador no tiene que configurar esos conceptos.

Un caso en el que usaría (sub)procesos para resolver un problema sería en una aplicación en Flutter escrita en Dart. Dart solo soporta `isolates` para tener paralelismo y para correr código en otro hilo que no sea el principal. Estos `isolates`, sin embargo, no comparten contexto, ni estado, ni memoria entre sí, por lo que puede ser considerados otro proceso diferente. Esta es la única oportunidad en la que he utilizado procesos para resolver un problema. 

Un caso para utilizar threads sería, por ejemplo, en una aplicación que tenga que hacer operaciones de lectura y escritura de manera intensiva y esto pueda causar un bloquéo de la aplicación. Para que el hilo principal no se bloquee mientras se carga la información, se puede usar uno o más hilos diferentes. Para este reto se podrían usar hilos para lanzar las peticiones, o para leer el archivo por partes si es demasiado grande.

Un caso para utilizar corrutinas sería al realizar varias actividades bloqueantes para el hilo, pero que sean ligeras y no ameriten lanzar un hilo nuevo. Por ejemplo, en este proyecto, al lanzar las peticiones de manera asíncrona, se están creando corrutinas para manejar la respuesta del servidor a cada petición. En este caso se pueden lanzar más de 40 peticiones y esperar la respuesta de todas ellas sin salir del hilo de ejecución.

> #### Optimización de recursos del sistema operativo
>
> Si tuvieras 1.000.000 de elementos y tuvieras que consultar para cada uno de ellos información en una API HTTP. ¿Cómo lo harías? Explicar.

Para este ejercicio pensé en que la solución debía ser hacer llamados a las APIs necesarias de manera asíncrona en un mismo proceso. Utilizar hilos cargaría demasiado a la máquina y realmente no ofrecería ninguna ventaja frente a usar llamados asíncronos. Es posible que se pueda llegar a una solución que utilice ambos acercamientos y combinarlos, pero eso no se probó y está fuera del alcance de esta discusión. 

A continuación el proceso de elección de estrategia para este ejercicio.

Este escenario es el escenario inicial haciendo llamados síncronos, sin utilizar ninguna estrategia de procesos, hilos ni corrutinas.



| Tamaño del archivo (filas) | Filas insertadas | Tiempo (s) |
| -------------------------: | ---------------: | ---------: |
| 9                          | 7                | 3          |
| 2000                       | 1902             | 558        |

> Nota: para cada fila se hacen al menos 2 peticiones, posiblemente 3.

Haciendo cálculos sencillos, 1 millón de peticiones tomarían 139.500 segundos en procesar, o 2.325 minutos, o 38.75 horas... Aproximadamente un día y medio.

Se realizaron tres experimentos para escoger una mejor forma de hacer estas peticiones. Se utilizaron librerías `asyncio` y `aiohttp` (A), `futures` y `threads` (B), `grequests` (C).

El experimento (A) no se pudo realizar ya que al intentar llamar al API de Mercado Libre con esta librería se recibía un error.

El experimento (B) tuvo los siguientes resultados con un consumo alto de recursos de la máquina:


| Tamaño del archivo (requests) | # conexiones | Tiempo (s)            |
| ----------------------------: | ------------:| --------------------: |
| 10                            | 100           | 0.21                 |
| 2000                          | 100           | 10.93                |
| 200000                        | 100           | 1118.02              |
| 1000000                       | 100           | 5590.1 (calculado)   |

 5590.1 segundos serían 93 minutos, o una hora y media.

> Nota: utilizar más de 100 conexiones simultáneas generaba errores, se probó con 150 y no fue posible hacer varios experimentos para promediar los tiempos. 

El experimento (C) utilizó `grequests`, que está basado en corrutinas para hacer llamados asíncronos.

| Tamaño del archivo (requests) | Tiempo (s)         |
| ----------------------------: | -----------------: |
| 10                            | 0.18               |
| 2000                          | 18.10              |
| 20000                         | 122.06             |
| 200000                        | 571.26             |
| 1000000                       | 2858.3 (calculado) |

2858.3 segundos son casi 48 minutos. `grequests` es la opción más prometedora.

También se realizaron mediciones de tiempo cargando el archivo de prueba de 2000 filas y haciendo cambios en el código para acoplar `grequests`.

Escenario A: sin cambios, llamados síncronos.

Escenario B: Se cambió el uso del API de ítems por uno que permite recibir 20 ítems en una sola petición. También se guardó en memoria la información de categorías que se iba recibiendo, dado que son categorías finitas, y además se consultó el endpoint para ver todas las categorías disponibles en un site. Todas las peticiones para recibir usuarios y categorías para cada ítem (que son la mayor parte de peticiones) para cada 20 ítems fueron enviadas asíncronamente.

Escenario C:  Se cargaron varias peticiones de 20 ítems en un lote de `grequests`, y las peticiones de usuarios y categorías para ese lote de múltiples ítems asíncronamente también. Este experimento cambió el tamaño del lote de ítems hasta dar con uno óptimo.

| Escenario | Tamaño del lote (filas) | Tiempo (s) |
| --------: | ----------------------: | ---------: |
| A         | -                       | 558        |
| B         | -                       | 89         |
| C         | 50                      | 70         |
| C         | 100                     | 67         |
| C         | 250                     | 66         |
| C         | 500                     | 70         |
| D         | 50                      | 55         |
| D         | 100                     | 48         |
| D         | 250                     | 40         |
| D         | 500                     | 51         |

Se escogió el escenario D con lotes de 250 filas al momento de leer el archivo.

`grequests` utiliza corrutinas, que fue finalmente más eficiente tanto en recursos como en rendimiento, mientras que `futures` utiliza hilos. No se probó una solución son (sub)procesos.

