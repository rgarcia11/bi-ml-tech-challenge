from desafio_tecnico.app.logic.file_handler.file_handler import FileHandler

PATH = 'data/test/technical_challenge_data_test'
PATH_WRONG = 'data/test/technical_challenge_data_test_wrong'
PATH_MIXED = 'data/test/technical_challenge_data_test_mixed'
ENCODING = 'UTF=8'
SEPARATOR = ','
CSV_FORMAT = '.csv'
JSONL_FORMAT = '.jsonl'
TXT_FORMAT = '.txt'


def test_csv_correct():
    read_configuration = {
        'path':      PATH,
        'format':    CSV_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == ['site', 'id']
    assert sites == {'MLA'}
    assert ids == list({'MLA750925229', 'MLA845041373', 'MLA693105237'})


def test_csv_wrong():
    read_configuration = {
        'path':      PATH_WRONG,
        'format':    CSV_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == None
    assert sites == None
    assert ids == None


def test_csv_mixed():
    read_configuration = {
        'path':      PATH_MIXED,
        'format':    CSV_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == ['site', 'id']
    assert sites == {'MLA'}
    assert ids == ['MLA750925229']


def test_txt_correct():
    read_configuration = {
        'path':      PATH,
        'format':    TXT_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == ['site', 'id']
    assert sites == {'MLA'}
    assert ids == list({'MLA750925229', 'MLA845041373', 'MLA693105237'})


def test_txt_wrong():
    read_configuration = {
        'path':      PATH_WRONG,
        'format':    TXT_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == None
    assert sites == None
    assert ids == None


def test_txt_mixed():
    read_configuration = {
        'path':      PATH_MIXED,
        'format':    TXT_FORMAT,
        'encoding':  ENCODING,
        'separator': SEPARATOR,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == ['site', 'id']
    assert sites == {'MLA'}
    assert ids == ['MLA750925229']


def test_jsonl_correct():
    read_configuration = {
        'path':     PATH,
        'format':   JSONL_FORMAT,
        'encoding': ENCODING,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)
    print(ids)
    assert headers == ['site','id']
    assert sites == {'MLA'}
    assert ids == list({'MLA750925229', 'MLA845041373', 'MLA693105237'})


def test_jsonl_wrong():
    read_configuration = {
        'path':     PATH_WRONG,
        'format':   JSONL_FORMAT,
        'encoding': ENCODING,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == None
    assert sites == None
    assert ids == None


def test_jsonl_mixed():
    read_configuration = {
        'path':     PATH_MIXED,
        'format':   JSONL_FORMAT,
        'encoding': ENCODING,
    }

    file_handler = FileHandler()
    (headers, sites, ids) = file_handler.handle_file(read_configuration)

    assert headers == ['site', 'id']
    assert sites == {'MLA'}
    assert ids == ['MLA750925229']
