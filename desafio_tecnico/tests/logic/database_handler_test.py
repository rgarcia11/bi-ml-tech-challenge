import psycopg2
import pytest

from desafio_tecnico.app.logic.database_handler import DatabaseHandler

TEST_ITEM = {
    'id':         'GALACTIC_ITEM',
    'site':       'GALACTIC_SITE',
    'price':      100,
    'currency':   'GALACTIC_PESOS',
    'start_time': None,
    'category':   'GALACTIC_CATEGORY',
    'seller':     'GALACTIC_SELLER',
}


@pytest.fixture
def postgres_connection():
    conn = psycopg2.connect(
        host="host.docker.internal",
        port='5432',
        dbname="postgres",
        user="postgres",
        password="secretisima")
    conn.set_session(autocommit=True)
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE biml_test
        (
            id TEXT,
            site TEXT,
            price INTEGER,
            currency TEXT,
            start_time TIMESTAMP,
            category TEXT,
            seller TEXT,
            PRIMARY KEY(id)
        );
    ''')

    cur.execute(f"""
        INSERT INTO biml_test VALUES(
            '{TEST_ITEM.get("id")}', 
            '{TEST_ITEM.get("site")}', 
            {TEST_ITEM.get("price")}, 
            '{TEST_ITEM.get("currency")}', 
            '{TEST_ITEM.get("start_time")}', 
            '{TEST_ITEM.get("category")}', 
            '{TEST_ITEM.get("seller")}')
        ON CONFLICT (id) DO NOTHING;
        """.replace("'None'", "NULL").replace("None", "NULL"))

    yield DatabaseHandler(conn=conn, database='biml_test')

    cur.execute('''
        DROP TABLE biml_test;
    ''')


def test_get_item(postgres_connection):
    db_item = postgres_connection.get_item(TEST_ITEM.get('id'))
    assert db_item == list(TEST_ITEM.values())


def test_delete_item(postgres_connection):
    postgres_connection.delete_item(TEST_ITEM.get('id'))
    item = postgres_connection.cur.execute(f'''
        SELECT * FROM biml_test WHERE id = '{TEST_ITEM.get("id")}';
    ''')
    assert item == None


def test_insert_item(postgres_connection):
    NEW_TEST_ITEM = TEST_ITEM.copy()
    NEW_TEST_ITEM['id'] = 'YET_ANOTHER_GALACTIC_ITEM'
    postgres_connection.insert_item(NEW_TEST_ITEM)

    db_item = postgres_connection.get_item(NEW_TEST_ITEM.get('id'))
    assert db_item == list(NEW_TEST_ITEM.values())
