# Desafío Técnico

Para correr este proyecto se necesita tener docker instalado.
Con correr dos comandos es suficiente:
1. `docker-compose build`
2. `docker-compose up`

Esto deja corriendo el API en `http://localhost:8080/`.

Para ver la documentación del API se puede ingresar a `http://localhost:8080/docs/`.

Para correr el ejercicio está el endpoint `https://localhost:8080/items` con el método POST. Puede correrse sin parámetros, o especificar un body en la petición de la siguiente manera:

```json
{
    "batch": "250",
    "encoding": "utf-8",
    "separator": ",",
    "format": ".jsonl",
    "path": "data/technical_challenge_data_small"
}
```

A continuación están las consideraciones que pueda tener cada punto en particular.

### Consigna
> El desafío técnico consiste en armar un servicio web que exponga un endpoint para leer un archivo, consultar una serie de APIs públicas de MercadoLibre y cargar una base de datos con los datos del archivo y las consultas a las APIs.

#### Microservicio
> - Se debe levantar usando **Flask**.
>
> - Debe exponer un endpoint que lance el ejercicio requerido. Puede haber más si se desea, pero no es necesario.

El endpoint, realizado con Flask, estará disponible tras correr `docker-compose build` y `docker-compose up` en `http://localhost:8080`. Ver `http://localhost:8080/docs/` para más información una vez esté corriendo el microservicio.

#### Lectura del archivo a cargar
> - El archivo consta de 2 columnas:
>
>     i. `site` (string): país de donde es el ítem (Argentina, Brasil). Cada site tiene un código; Argentina es ​MLA​, Brasil es M​ LB​, etc.
>
>     ii. `id` (numérico): código de identificación del ítem. La clave de un ítem consta del site y el id concatenado. Si el site es “FOO” y el id es “123”, la clave de un ítem es “FOO123”.

> - La lectura del archivo se debe poder configurar, y debe ser parte del programa (formato, separador, enconding, etc). Si se cambiara algo del archivo (por ejemplo, separador o formato), debe poder leerse cambiando la configuración.

> - Debe soportar formatos streameables (ej.: csv, jsonlines, txt). Formatos como JSON que no lo son, pueden no ser soportados; no es parte del ejercicio.

> - En el ejemplo son pocas líneas. Trabajarlo como si fuera un archivo más grande que la memoria.

#### Consulta a las APIs
> - Debe hacerse con la premisa de la lectura del archivo: realizar las consultas de la manera más rápida posible, de la manera más eficiente en cuanto a uso de recursos del sistema operativo.

Para cuidar los recursos del sistema operativo no se pensó en una solución con hilos, por ejemplo. Se probó pero rápidamente se vio el alto consumo de recursos. Se optó por realizar varias optimizaciones al momento de llamar a las APIs y hacer los llamados por lotes de manera asíncrona.

La primera optimización fue reducir el número de llamados a las APIs. Por intuición, y según la documentación, el número de categorías es finito por región. Por tanto, se llama al API, por ejemplo, `https://api.mercadolibre.com/sites/MLA/categories`, y se guardan todas las categorías en memoria para todas las regiones que haya en el archivo. Si hay algún ítem cuya categoría no esté en la colección que se formó, se consulta con `https://api.mercadolibre.com/categories/<category_id>` y se guarda en memoria.

La segunda optimización fue similar pero con monedas. Se llama el API `https://api.mercadolibre.com/currencies` y se guarda en memoria la información de las monedas.

La tercera es utilizar el API `https://api.mercadolibre.com/items?ids=<id1>,<id2>...<id20>` que soporta hasta 20 ítems diferentes en una sola petición.

Por último, se forman lotes de peticiones, primero un lote de peticiones para obtener la información de los ítems, luego dos lotes de peticiones para categorías (de ser necesario) y para usuarios. Se esperan resultados y se forman los registros a insertar en base de datos.

> - Desarrollar suponiendo que se podrían agregar nuevas APIs para consultar.

Agregar una nueva API sería sencillo. Habría que editar únicamente el RequestHandler.

> - Flujo de consumo de APIs (todas las APIs son públicas de MercadoLibre, documentación en ​API Docs​):
>     - Con la data del archivo, consultar la API de Ítems de MercadoLibre. A partir de la respuesta:
>         - Quedarse con el campo `price`
>         - Quedarse con el campo `start_time`
>         - Con el `category_id` pegarle a la api de categorías y traerse el campo `name`
>         - Con el `currency_id` pegarle a la api de currencies y traerse el campo `description`
>         - Con el `seller_id` pegarle a la api de users y traerse el campo `nickname`
        
#### Base de datos
> - Los campos a insertar finalmente van a ser:
>     - `site` (obtenido del file)
>     - `id` (obtenido del file)
>     - `price` (obtenido de la api de ítems)
>     - `start_time` (obtenido de la api de ítems)
>     - `name` (obtenido de la api de categorías)
>     - `description` (obtenido de la api de monedas)
>     - `nickname` (obtenido de la api de users)

Se creó la base de datos en PostgreSQL con esta estructura:
```sql
 CREATE TABLE biml
    (
        id TEXT,
        site TEXT,
        price INTEGER,
        currency TEXT,
        start_time TIMESTAMP,
        category TEXT,
        seller TEXT,
        PRIMARY KEY(id)
    );
```
Se cambió el nombre `description` por `currency`, `nickname` por `seller` y `name` por `category`.

> - Se debe levantar en un container. El motor debe ser relacional, y es a criterio del desarrollador.

Es una base de datos relacional. Los comandos `docker-compose build` y `docker-compose up` deberían bastar para dejar la base de datos corriendo.

> - No es necesario escribir el código del container. Se puede utilizar una imagen existente. A criterio del desarrollador.

La imagen utilizada es [postgres](https://hub.docker.com/_/postgres).

> - Se debe poder acceder a la tabla cargada con los datos.

Para acceder a ella se puede usar el comando `psql -h localhost -p 5432 -U postgres` y la clave `secretisima`, pero hay que tener postgres (y psql) instalado.

#### Documentación
> - Se debe entregar la documentación necesaria para ejecutar el challenge de manera fluida y entender el proyecto. Cuánta documentación es necesaria queda a criterio del desarrollador.

Aparte de la documentación del código, se puede acceder a la documentación del API en Swagger en la url `http://localhost:8080/docs/` una vez esté corriendo el microservicio.

#### Testing
> - No son necesarios para el desafío. Si se conoce un framework de testing, implementar tests unitarios simples para demostrar el nivel de conocimiento del framework.


> ### ¿Qué se va a evaluar?
>
> #### En general
> - Uso de módulos y librerías: evitar usar librerías cuando un módulo built-in lo puede resolver.
> - Buenas prácticas de código y patrones de diseño.
>
> #### Microservicio
> - Estructura del proyecto. Flexibilidad (“​que los cambios no duelan​”) y modularidad (separación de responsabilidades).
>
> #### Lectura del archivo
> - Construcción de buenas abstracciones para que sea fácil soportar múltiples formatos a través de configuración.
> - Evitar ​spaghetti code
>
> #### Consulta a las APIs
> - Selección de una manera eficiente en cuanto a recursos y rápida de interactuar con las APIs.
>
> #### Base de datos
> - Elección del motor. Hay múltiples opciones y ninguna es necesariamente mejor que otra. Elegir la que más cómoda
>
> #### Documentación
> - Claridad suficiente para poder ejecutar la solución punta a punta, desde el build del container hasta la carga de la base de datos.
> - La buena documentación es importante para el mantenimiento y el trabajo en equipo, así que es algo a lo que le vamos a dar importancia.
>
> #### Testing
> - Opcional, pero suma puntos :) .