import os

from desafio_tecnico.app.logic.file_handler.format_extractors.format_extractors import info_extractors, file_openers, \
    available_formats


class FileHandler:
    """
    Class to handle all interactions with the file to be read
    """

    CSV_LOCATION = './data/technical_challenge_data_small.jsonl'

    def initialize_readers(self, file_extension):
        """Initializes the functions necessary to read a file, depending on the format.
        It uses the format_extractors.py file to choose the right reading strategy"""
        open_file = file_openers.get(file_extension)
        if open_file:
            self._open_file = open_file
        extract_info = info_extractors.get(file_extension)
        if extract_info:
            self._extract_info = extract_info

    def _open_file(self, file_path: str, encoding: str) -> list:
        """Default file opener function. Only to be used when the file format isn't recognized"""
        return None

    def _extract_info(self, rows: list, separator: str) -> tuple:
        """Default info extractor function. Only to be used when the file format isn't recognized"""
        return None, None, None

    def handle_file(self, read_configuration):
        """Reads file onto memory and returns a tuple with usable information
        Parameters
        ----------
        read_configuration : dict
            Dictionary containing the requests' body with read configurations.
            Encoding, format, path, and separator are used.
        """
        # TODO Handle a file larger than memory
        file_path = read_configuration.get('path') if read_configuration.get('path') else self.CSV_LOCATION

        file_name, file_extension = os.path.splitext(file_path)
        if not file_extension:
            if read_configuration.get('format'):
                file_extension = read_configuration.get('format')

        self.initialize_readers(file_extension)
        rows = self._open_file(f'{file_name}{file_extension}', read_configuration.get('encoding'))
        return self._extract_info(rows, read_configuration.get('separator'))
