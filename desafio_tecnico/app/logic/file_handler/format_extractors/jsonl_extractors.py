import json


def jsonl_file_opener(file_path, encoding: str = 'utf-8'):
    """Opens a CSV file as a list

    Parameters
    ----------
    file_path : str
        Path to the file to open
    encoding : str
        Defaults to 'utf-8'. Encoding to open the file with.

    Returns
    -------
    rows : list
        List of rows in the file
    """
    if not encoding:
        encoding = 'utf-8'
    with open(file_path, encoding=encoding) as file:
        rows = list(map(str.strip, file))
    try:
        return [json.loads(jline) for jline in rows]
    except:
        return []



def jsonl_info_extractor(rows, _):
    """Given a list of rows in the format '{"site": <site>, "id": <id>}', extracts all sites and all ids as a tuple

    Parameters
    ----------
    rows : list
        List of rows in a file in the form f'site{separator}id`
    separator : str, ignored

    Returns
    -------
    (headers, sites, ids) : tuple
        A tuple containing the headers, a set with all unique sites, and a list unique with ids
    """
    if not rows:
        return None, None, None
    ids = set()
    sites = set()
    headers = list(rows[0].keys())

    for row in rows:
        site = row.get("site")
        iid = row.get("id")
        if site:
            sites.add(site)
            if iid:
                ids.add(f'{site}{iid}')

    return headers, sites, list(ids)
