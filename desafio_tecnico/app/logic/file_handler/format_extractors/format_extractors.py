"""
Helper file containing all available formats and two dictionaries that point to each
specific file opener and info extractor functions, depending on the format to be read.

"""
from desafio_tecnico.app.logic.file_handler.format_extractors.csv_txt_extractors import csv_txt_file_opener, \
    csv_txt_info_extractor
from desafio_tecnico.app.logic.file_handler.format_extractors.jsonl_extractors import jsonl_file_opener, \
    jsonl_info_extractor

CSV = '.csv'
TXT = '.txt'
JSONL = '.jsonl'

available_formats = [CSV, TXT, JSONL]

file_openers = {
    CSV:   csv_txt_file_opener,
    JSONL: jsonl_file_opener,
    TXT:   csv_txt_file_opener,
}

info_extractors = {
    CSV:   csv_txt_info_extractor,
    JSONL: jsonl_info_extractor,
    TXT:   csv_txt_info_extractor,
}
