def csv_txt_file_opener(file_path: str, encoding: str = 'utf-8') -> list:
    """Opens a CSV or TXT file as a list

    Parameters
    ----------
    file_path : str
        Path to the file to open
    encoding : str
        Defaults to 'utf-8'. Encoding to open the file with.

    Returns
    -------
    rows : list
        List of rows in the file
    """
    if not encoding:
        encoding = 'utf-8'
    with open(file_path, encoding=encoding) as file:
        rows = list(map(str.strip, file))
    return rows


def csv_txt_info_extractor(rows: list, separator: str = ',') -> tuple:
    """Given a list of rows in the format 'site,id', extracts all sites and all ids as a tuple

    Parameters
    ----------
    rows : list
        List of rows in a file in the form f'site{separator}id`
    separator : str
        Separator between the values in a row

    Returns
    -------
    (headers, sites, ids) : tuple
        A tuple containing the headers, a set with all unique sites, and a list with unique ids
    """
    if not separator:
        separator = ','
    ids = set()
    sites = set()
    headers = rows[0].split(separator)

    for row in rows[1:]:
        split = row.split(separator)
        if len(split) != 2:
            return None, None, None
        site = split[0]
        iid = split[1]
        if site:
            sites.add(site)
            if iid:
                ids.add(f'{site}{iid}')

    return headers, sites, list(ids)
