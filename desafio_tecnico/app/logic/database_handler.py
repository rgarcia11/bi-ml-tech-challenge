import psycopg2
import psycopg2.extras


class DatabaseHandler:
    """
    Class to handle all interactions with the database
    """

    def __init__(self, conn=None, database: str = None):
        self.conn = conn
        if not self.conn:
            self.conn = psycopg2.connect(
                host="host.docker.internal",
                port='5432',
                dbname="postgres",
                user="postgres",
                password="secretisima")
        self.conn.set_session(autocommit=True)
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.database = database
        if not self.database:
            self.database = 'biml'

    def get_item(self, iid):
        self.cur.execute(f"SELECT * FROM {self.database} WHERE id = '{iid}'")
        item = self.cur.fetchone()
        return item

    def delete_item(self, iid):
        """Delete all items"""
        self.cur.execute(f"DELETE FROM {self.database} WHERE id = '{iid}'")

    def insert_item(self, item):
        """Create item"""
        print(item)
        self.cur.execute(f"""
            INSERT INTO {self.database} VALUES(
                '{item.get("id")}', 
                '{item.get("site")}', 
                {item.get("price")}, 
                '{item.get("currency")}', 
                '{item.get("start_time")}', 
                '{item.get("category")}', 
                '{item.get("seller")}')
            ON CONFLICT (id) DO NOTHING;
            """.replace("'None'", "NULL").replace("None", "NULL"))

        return f'{item}'

    def get_all(self) -> list:
        """Get all items"""
        self.cur.execute(f'SELECT * FROM {self.database}')
        return self.cur.fetchall()

    def delete_all(self) -> list:
        """Delete all items"""
        self.cur.execute(f'DELETE FROM {self.database} WHERE true')

    def insert_rows_execute_batch(self, rows) -> None:
        """Insert rows to database using the execute_batch method"""
        # TODO Failure handling
        with self.conn.cursor() as cursor:
            psycopg2.extras.execute_batch(cursor, f"""
                INSERT INTO {self.database} VALUES (
                    %(id)s,
                    %(site)s,
                    %(price)s,
                    %(currency)s,
                    %(start_time)s,
                    %(category)s,
                    %(seller)s
                )
                ON CONFLICT (id) DO NOTHING;
            """, rows)

    def get_all(self) -> list:
        """Get all items"""
        self.cur.execute(f'SELECT * FROM {self.database}')
        return self.cur.fetchall()

    def delete_all(self) -> list:
        """Delete all items"""
        self.cur.execute(f'DELETE FROM {self.database} WHERE true')

    def terminate(self):
        """Terminate the connection to the database"""
        self.conn.close()
