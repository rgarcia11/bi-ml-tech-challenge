import grequests
import requests

from desafio_tecnico.app.logic.routes import BASE, CATEGORIES, CURRENCIES, ITEMS, SITES, USERS


class RequestsHandler:
    """
    Class to handle all requests and calls to APIs needed by this project.
    """

    CURRENCY_INFORMATION = {}
    CATEGORY_INFORMATION = {}

    def initialize(self, sites: set) -> None:
        """Initializes categories for a given set of sites and all currencies as dictionaries:
        CURRENCY_INFORMATION = {'currency_id': 'currency description'}
        CATEGORY_INFORMATION = {'category_id': 'category name'}

        Parameters
        ----------
        sites : set
            Set containing all unique sites to fetch for categories
        """
        for site in sites:
            categories = self._call_endpoint(f'{SITES}/{site}/{CATEGORIES}')
            if type(categories) is list:
                for category in self._call_endpoint(f'{SITES}/{site}/{CATEGORIES}'):
                    self.CATEGORY_INFORMATION[category.get('id')] = category.get('name')

        for currency in self._call_endpoint(CURRENCIES):
            self.CURRENCY_INFORMATION[currency.get('id')] = currency.get('description')

    def handle_rows(self, item_ids: list) -> list:
        """Fetches all the APIs needed for each id in a given list of ids,
        returns all items to be inserted into the database.

        - Every 20 item ids, a request is formed to fetch the information of those 20 items.
        - Categories are looked for in self.CATEGORY_INFORMATION first, and if not found,
        they are fetched from the API and saved in self.CATEGORY_INFORMATION.
        - Currencies are looked for in self.CURRENCY_INFORMATION.
        - Sellers are fetched from the API.

        All requests are done in batches.

        Parameters
        ----------
        item_ids : list
            List of item ids to fetch info from the APIs and insert into the database

        Returns
        -------
        new_rows : list
            List of rows to be inserted into the database.
        """
        # extract items in 20-item requests and as many batches as needed
        items = self._extract_items(item_ids)

        # extract all the endpoints needed for those items
        (category_endpoints, seller_endpoints) = self._extract_endpoints(items)

        category_responses = self._call_endpoints_async(category_endpoints)
        for category in category_responses:
            self.CATEGORY_INFORMATION[category.get('id')] = category.get('name')

        seller_responses = self._call_endpoints_async(seller_endpoints)
        seller_info = {}
        for seller in seller_responses:
            seller_info[seller.get('id')] = seller.get('nickname')

        # form the new rows
        new_rows = []

        for item in items:
            if item.get('code') == 200:
                item_dict = item.get('body')
                new_row = {
                    'id':         item_dict.get('id'),
                    'site':       item_dict.get('site_id'),
                    'price':      item_dict.get('price'),
                    'currency':   self.CURRENCY_INFORMATION.get(item_dict.get('currency_id')),
                    'start_time': item_dict.get('start_time'),
                    'category':   self.CATEGORY_INFORMATION.get(item_dict.get('category_id')),
                    'seller':     seller_info.get(item_dict.get('seller_id'))
                }
                new_rows.append(new_row)

        return new_rows

    def _extract_items(self, item_ids: list) -> list:
        """Helper function to call the items API every 20 items.

        Parameters
        ----------
        item_ids : list
            List of items to fetch from the API

        Returns
        -------
        items : list
            All items fetched
        """
        start_index = 0
        end_index = 20
        items_endpoints = []
        while start_index < len(item_ids):
            items_endpoints.append(f'{ITEMS}?ids={",".join(item_ids[start_index:end_index])}')
            start_index += 20
            end_index += 20

        item_responses = self._call_endpoints_async(items_endpoints)
        items = []
        for item_results in item_responses:
            for item in item_results:
                items.append(item)
        return items

    def _extract_endpoints(self, items: list) -> tuple:
        """Helper function to form the requests for categories and sellers as needed
        Parameters
        ----------
        items : list
            List of items (as dictionaries)

        Returns
        -------
        (category_endpoints, seller_endpoints) : tuple
            All the endpoints for categories and sellers to be fetched from the API
        """
        category_endpoints = []
        seller_endpoints = []

        for item in items:
            if item.get('code') == 200:
                item_dict = item.get('body')
                category = self.CATEGORY_INFORMATION.get(item_dict.get('category_id'))
                if not category and item_dict.get('category_id'):
                    category_endpoints.append(f'{CATEGORIES}/{item_dict.get("category_id")}')
                seller_id = item_dict.get('seller_id')
                if seller_id:
                    seller_endpoints.append(f'{USERS}/{seller_id}')

        return (category_endpoints, seller_endpoints)

    def _call_endpoint(self, endpoint: str) -> requests.Response:
        """Helper function to call an endpoint synchronously

        Parameters
        ----------
        endpoint : str
            Endpoint to call

        Returns
        -------
        requests.Response response object
        """
        # TODO Handle requests error (code not 200 and whatnot)
        return requests.get(f'{BASE}/{endpoint}').json()

    def _call_endpoints_async(self, endpoints: list) -> list:
        """Helper function to call a list of endpoints asynchronously

        Parameters
        ----------
        endpoints : list
            List of endpoints to call (the complete batch of endpoints to call)

        Returns
        -------
        responses : list
            List of responses as dictionaries
        """
        requests_generator = (grequests.get(f'{BASE}/{endpoint}') for endpoint in endpoints)
        responses = grequests.map(requests_generator)
        return [response.json() for response in responses]
