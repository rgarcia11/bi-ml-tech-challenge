"""
File for keeping track of all routes and avoid passing raw strings as parameters
"""

BASE = 'https://api.mercadolibre.com'

CATEGORIES = 'categories'
CURRENCIES = 'currencies'
ITEMS = 'items'
USERS = 'users'
SITES = 'sites'
