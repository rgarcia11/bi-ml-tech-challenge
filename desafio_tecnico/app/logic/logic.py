import time

from desafio_tecnico.app.logic.database_handler import DatabaseHandler
from desafio_tecnico.app.logic.file_handler.file_handler import FileHandler
from desafio_tecnico.app.logic.requests_handler import RequestsHandler


class Logic:
    """
    Class to handle all the other classes in the package.
    """

    def __init__(self):
        """Initialize all handlers"""
        self.db_handler = DatabaseHandler()
        self.file_handler = FileHandler()
        self.rq_handler = RequestsHandler()

    def read_db(self) -> list:
        """Get all items

        Returns
        -------
        list containing all database entries
        """
        return self.db_handler.get_all()

    def get_item(self, iid) -> dict:
        """Get item by iid

        Parameters
        ----------
        iid : str
            item id to fetch

        Returns
        -------
        dict containing the item that was added
        """
        return self.db_handler.get_item(iid)

    def delete_db(self) -> None:
        """Delete all items"""
        self.db_handler.delete_all()

    def delete_item(self, iid) -> None:
        """Delete single item
        Parameters
        ----------
        iid : str
            item id to delete
        """
        return self.db_handler.delete_item(iid)

    def create_item(self, item: dict) -> str:
        """Create item

        Parameters
        ----------
        item : dict
            dict containing the item to insert

        Returns
        -------
        str with either an error message or the inserted item
        """
        if item.get('id') is not None:
            return self.db_handler.insert_item(item)
        else:
            return 'An id is necessary.'

    def file_to_database(self, read_configuration: dict) -> list:
        """Read ids from a file, fetch information, and insert to the database

        Parameters
        ----------
        read_configuration : dict
            Configuration for handling the file, including
            batch size (simultaneous requests), encoding, format, path, separator.

        Returns
        -------
        str containing how many items were actually inserted in the database and how long it took
        """
        time1 = time.time()

        # Read file data
        (headers, sites, ids) = self.file_handler.handle_file(read_configuration)
        if sites is None or ids is None:
            return f'An error occurred. Format is probably not recognized, or file is empty.'

        # Pre load important information
        self.rq_handler.initialize(sites)

        # Cut up the file in 500-row batches, run them through the APIs and insert into DB
        batch_size = int(read_configuration.get('batch')) if read_configuration.get('batch') else 250
        start_index = 0
        end_index = batch_size
        while start_index < len(ids):
            new_rows = self.rq_handler.handle_rows(ids[start_index:end_index])
            self.db_handler.insert_rows_execute_batch(new_rows)
            start_index += batch_size
            end_index += batch_size

        time2 = time.time()

        return f'Loaded {len(ids)} into Postgres in {time2 - time1} seconds!'
