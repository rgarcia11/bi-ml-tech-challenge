# Lines necessary to avoid an error involving the grequests package and Flask
from gevent import monkey

monkey.patch_all()

from flask import Flask, request, jsonify
from flask_swagger_ui import get_swaggerui_blueprint
from desafio_tecnico.app.logic.logic import Logic

flask_app = Flask(__name__)
logic = Logic()

# -----||-----
# Swagger info
SWAGGER_URL = '/docs'
API_URL = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "BI-ML-Tech-Challenge"
    }
)
flask_app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


# -----||-----

@flask_app.route("/items", methods=['POST'])
def insert_file_into_database():
    """Launches this project's objective: read ids from a file, fetching information, and inserting to a database"""
    read_configuration = request.json
    if type(read_configuration) is not dict:
        read_configuration = {}
    return jsonify(logic.file_to_database(read_configuration))


@flask_app.route("/items", methods=['GET'])
def get_items():
    """Get all items"""
    return jsonify(logic.read_db())


@flask_app.route("/items", methods=['DELETE'])
def delete_items():
    """Delete all items"""
    return jsonify(logic.delete_db())


@flask_app.route("/item", methods=['POST'])
def create_item():
    """Create a single item"""
    item = request.json
    if type(item) is not dict:
        return jsonify({"message": "A body containing an item is necessary."})
    return jsonify(logic.create_item(request.json))


@flask_app.route("/item/<iid>", methods=['DELETE'])
def delete_item(iid):
    """Delete item"""
    if iid is None:
        return jsonify({"message": "An id is necessary."})
    jsonify(logic.delete_item(iid))
    return jsonify({"message": f"Item {iid} deleted"})


@flask_app.route("/item/<iid>", methods=['GET'])
def get_item(iid):
    """Get a single item"""
    if iid is None:
        return jsonify({"message": "An id is necessary."})
    return jsonify(logic.get_item(iid))


if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', port=8080)
